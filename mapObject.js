function mapObject(testObject, cb) {
  if (
    typeof testObject === "object" &&
    testObject != null &&
    !Array.isArray(testObject)
  ) {
    const newObject = {};

    for (let index in testObject) {
      newObject[index] = cb(testObject[index], index);
    }

    return newObject;
  } else {
    return "Passed data is not an object";
  }
}

module.exports = mapObject;
