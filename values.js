function values(objectData) {
  const valuesArray = [];
  if (
    typeof objectData === "object" &&
    typeof objectData !== null &&
    !Array.isArray(objectData)
  ) {
    for (let values in objectData) {
      valuesArray.push(objectData[values]);
    }

    return valuesArray;
  } else {
    return "Passed data is not an object";
  }
}

module.exports = values;
