function defaults(objectData,defaults){
    if(typeof(objectData) === "object" && objectData != null && !(Array.isArray(objectData))){

        for(let index in defaults){
            if(objectData[index]===undefined){
                objectData[index]=defaults[index];
            }
        }
    
        return defaults;
    }
    else{
        return ("Data is not an Object")
    }
    
}

module.exports=defaults;
