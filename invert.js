function invert(testObject) {
  const valuesArray = [];
  if (typeof testObject === "object" && typeof testObject !== null && !Array.isArray(testObject)){

    const newObject = {};

    for (let key in testObject) {
      newObject[testObject[key]] = key;
    }

    return newObject;
    
  } else {
    return "Passed data is not an object";
  }
}

module.exports = invert;
