function pairs(testObject) {
  if (
    typeof testObject === "object" &&
    testObject != null &&
    !Array.isArray(testObject)
  ) {
    const newArray = [];

    for (let key in testObject) {
      newArray.push([key, testObject[key]]);
    }
    return newArray;
  } else {
    return "Passed data is not an Object";
  }
}
module.exports = pairs;
