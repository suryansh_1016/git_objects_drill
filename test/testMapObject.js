const mapObject = require("../mapObject");
const testObject = require("../testObject");

const result = mapObject(testObject, (element, index) => {
  if (typeof element === "string") {
    return element.toUpperCase();
  }
  return element;
});

console.log(result);
