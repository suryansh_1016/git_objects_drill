const objectData = require("../testObject");
const defaults = require("../defaults");

const defaultsObject = { ...objectData, profession: "Superhero" };

const result = defaults(objectData, defaultsObject);

console.log(result);
