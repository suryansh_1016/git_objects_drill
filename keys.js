function keys(objectData) {
  const keyArray = [];
  if (
    typeof objectData === "object" &&
    typeof objectData !== null &&
    !Array.isArray(objectData)
  ) {
    for (let key in objectData) {
      keyArray.push(key);
    }
    return keyArray;
  } else {
    return "Passed data is not an object";
  }
}

module.exports = keys;

